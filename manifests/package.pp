# Class: apache::package
#
# This module manages the apache package.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class apache::package {
  
  package { 'httpd':
  ensure => 'latest',
  }
  
  package { 'httpd-tools':
  ensure => 'latest',
  }
  
}
