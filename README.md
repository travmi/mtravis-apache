#APACHE

####Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with apache](#setup)
    * [What apache affects](#what-apache-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with apache](#beginning-with-apache)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

##Overview

The APACHE module installs, configures, and manages the APACHE service.

##Module Description

The APACHE module handles installing, configuring, and running APACHE on CentOS 6.5+.	 

##Setup

###What apache affects
* apache package.
* apache configuration file.
* apache service.

###Beginning with APACHE

`include 'apache'` is enough to get you running but if you want more control
you can do the following as a resource-like decleration. I highly recommend
using `include` and put your parameters into Hiera.

```puppet
class { 'apache':
  port 				   => 8080,
  url                  => 'puppettest.wcw.local',
  keepalive_enable     => off,
  maxkeepaliverequests => 500,
  keepalivetimeout     => 60,
  server_name          => 'puppettest.wcw.local',
  client_folder        => puppettest
}
```
You can use the defined type `apache::vhost` to create virtual host files.
Once created it will trigger the apache service to restart.

```puppet
 apache::vhost { 'foobar.wcw.local':
                port          => 8080,
                url           => 'foobar.wcw.local',
                server_name   => 'foobar.wcw.local',
                client_folder => foobar,
                server_alias  => foobar,
                error_log     => foobar_error_log,
                access_log    => foobar_access_log
        }
```


##Usage

All interaction with the apache module can do be done through the main apache class.

###I just want apache, what's the minimum I need?

```puppet
include apache
```

##Reference

###Classes

####Public Classes

* apache: Main class, includes all other classes.

####Private Classes

* apache::install: Handles the packages.
* apache::config: Handles the configuration files.
* apache::service: Handles the service.
* apache::params: Handles all the case variables for the classes.

####Defined Types

* apache::vhost: Handles the virtual hosts file.

###Parameters for apache

The following parameters are available in the apache module:

####`port`

####`url`

####`keepalive_enable`

####`maxkeepaliverequests`

####`keepalivetimeout`

####`server_name`

####`client_folder`

###Parameters for apache::vhost

####`port`
This is the port in which you want the virtual host to listen on.

####`url`
The url of the site.

####`server_name`
Name of the server will be the same as the url.

####`client_folder`
Short name of the client.

####`server_alias`
Array of values for the ServerAlias directive in the vhost file.
```puppet
    server_alias         => [ 'example.com',
                              'www.example.com',
                              'example.org',
                              'www.example.org' ]
```

####`error_log`
Name of the error log.

####`access_log`
Name of the access log.

####`error_doc`
When set to true it will add the following to the vhost file.
Defaults to false.
```puppet
ErrorDocument 404 /404.html
```

####`logstash_log_forward`
When set to true it will append 'logstash' to the end of the access log.
Defaults to false.
```
CustomLog logs/<%= @access_log %> logstash_forwarded
```

####`options`
When set to true it will have the following in the config.
```
        <Directory "/var/www/<%= @client_folder %>/<%= @url %>/htdocs/">
				Options -All +SymLinksIfOwnerMatch
                AllowOverride All
        </Directory>
```

####`manage_dir`
When set to true it will manage permissions and ownership for the following directories.
Defaults to false.
```
/var/www/${client_folder}
/var/www/${client_folder}/${url}
```

##Limitations

This module has been built on and tested against Puppet Enterprise 3.3 and higher.

The module has been tested on:

* CentOS 6.5+

Testing on other platforms has not been and is currently not supported. 

##Development

###Contributors

Mike Travis - <mtravis@webcourseworks.com>


