# == Class: apache
#
# This class manages apache.
#
# === Parameters
#
# [*port*]
#   This sets the port that Apache listens. Default is 8080.
#   
# [*url*]
#   The URL of the site. This is also used in setting the document
#   root of the site. Default is currently test.wcw.local.
#
# [*keepalive_enable*]
#
# [*maxkeepaliverequests*]
#
# [*keepalivetimeout*]
#
# [*server_name*]
#
# [*client_folder*]
#   This is the name of the client. It will be used to set
#   the document root in httpd.conf. Default is test.
#
# === Examples
#
#
# === Authors
#
# Author Name <mtravis@webcourseworks.com>
#
# === Copyright
#
# Copyright 2014 Web Courseworks Ltd.
#
class apache (
  
  $port                 = 8080,
  $url                  = "${::fqdn}",  
  $keepalive_enable     = off,
  $maxkeepaliverequests = 500,
  $keepalivetimeout     = 60,
  $server_name          = "${::fqdn}",
  $client_folder        = local
    
) {

  validate_string($url)
  validate_string($client_folder)
  validate_string($server_name)

#  $dirs_present = [ "/var/www/${client_folder}/",
#                    "/var/www/${client_folder}/${url}/" ]

#  file { $dirs_present:
#    ensure => 'directory',
#    owner  => 'apache',
#    group  => 'apache',
#    mode   => '0550'
#  }

# Not sure if anchor is needed or if it was deprecated  
 anchor { 'apache::begin': } ->
    class  { '::apache::package': } ->
    class  { '::apache::config': } ~>
    class  { '::apache::service': } ->
 anchor { 'apache::end': }

}
