# Defined Type: apache::vhost
#
# This module manages apache virtual hosts.
#
# Parameters:
#
# [*port*]
# Define the port that the virtual host will list on.
#
# [*url*]
# The url of the site.
#
# [*server_name*]
# The server name.
#
# [*client_folder*]
# Used as part of the document root
# Example: /var/www/client/client_url
#
# [*error_log*]
# The name of the error log.
#
# [*access_log*]
# The name of the access log.
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
define apache::vhost (
  
  $port                 = 8080,                 
  $url                  = undef,                  
  $server_name          = undef,         
  $client_folder        = undef,
  $server_alias         = undef,
  $error_log            = undef,
  $access_log           = undef,
  $error_doc            = false,
  $logstash_log_forward = false,  
  $options              = false,
  $manage_dir           = false,
  
) {
   
  validate_string($url)
  validate_string($server_name)
  validate_string($client_folder)
  validate_array($server_alias)
  validate_string($error_log)
  validate_string($access_log)
  validate_bool($error_doc)
  validate_bool($logstash_log_forward)
  validate_bool($options)
  validate_bool($manage_dir)
  
  if ! defined(Class['apache']) {
    fail('You must include the apache base class before using any apache defined resources')
  }

  if $manage_dir {
  $dirs = [ "/var/www/${client_folder}/",
            "/var/www/${client_folder}/${url}" ]

  file { $dirs:
    ensure => 'directory',
    owner  => 'apache',
    group  => 'apache',
    mode   => '0550',
    }
  }

  file { "/etc/httpd/conf.d/${url}.conf":
    ensure  => 'file',
    content => template('apache/vhost.conf.erb'),
    mode    => '0644',
    notify  => Service['httpd']
  }

}
