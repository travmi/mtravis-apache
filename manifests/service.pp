# Class: apache::service
#
# This module manages apache service.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class apache::service {
 
 service { 'httpd':
  ensure => 'running',
  enable => 'true',
  }
    
}
