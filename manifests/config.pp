# Class: apache::config
#
# This module manages apache configuration files.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class apache::config {
  
#  $dirs_absent = [ "/var/www/cgi-bin", 
#                   "/var/www/html", ]

#  file { $dirs_absent:
#    ensure => 'absent',
#    force  => true,
#  }
  
  file { '/etc/httpd/conf/httpd.conf':
    ensure  => 'file',
    content => template('apache/httpd.conf.erb'),
    mode    => '0644',
  }

  file { '/etc/httpd/conf.d':
    ensure  => 'directory',
    recurse => true,
    purge   => true
  }

  file { '/etc/httpd/conf.d/php.conf':
    ensure => 'file',
    source => "puppet:///modules/apache/php.conf",
    mode   => '0644',
    notify => Service['httpd']
  }

  file { '/etc/logrotate.d/httpd':
    ensure => 'file',
    source => "puppet:///modules/apache/httpd",
    mode   => '0644'
  }
 
}
